﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CursoV3Ejercicio1
{
    class User
    {
        string nombre;
        string apellido;
        int age;
        string genre;
        string sexo;


        public User(string nombre)
        {
            this.nombre = nombre;
            if (nombre.Equals("Jaume"))
            {
                apellido = "marques";
                age = 25;
                sexo = "No homo.";
                genre = "Masculino";
            }
            else if (nombre.Equals("Victor R"))
            {
                apellido = "Rocha";
                age = 3;
                sexo = "Con Jaume";
                genre = "Masculino";
            }
            else if (nombre.Equals("Victor A"))
            {
                apellido = "Arnedo";
                age = 25;
                sexo = "poco y malo";
                genre = "Masculino";
            }
            else if (nombre.Equals("Pablo"))
            {
                apellido = "Ferrari";
                age = 25;
                sexo = "(menos que) a menudo";
                genre = "Masculino";
            }
            else if (nombre.Equals("Ivan"))
            {
                apellido = "rodriguez";
                age = 31;
                sexo = "never";
                genre = "Masculino";
            }
        }


        public override string ToString()
        {
            string res = "nombre : " + this.nombre + "\n" +
                "apellido : " + this.apellido + "\n" +
                "age : " + this.age + "\n" +
                "sexo : " + this.sexo + "\n" +
                "genre : " + this.genre + "\n";

            return res;
        }
    }
}
